# uniapp-image-edit

#### 介绍

基于uniapp的H5图片编辑器
详情可查看：https://www.cnblogs.com/my-wl/p/17129858.html

#### 使用说明

1.  components目录下为图片编辑组件
2.  config下为全局事件声明
3.  使用时只需将组件copy到HBuilderX项目当中作为组件使用即可
4.  组件在打开与保存关闭时会抛出`GLOBAL_HIDE_TABBAR`事件，需在外部监听事件，控制tabbar的显示与隐藏

通过$refs调用组件的open方法，传图图片url即可打开图片编辑
通过监听ok事件，可以获取保存后的图片url
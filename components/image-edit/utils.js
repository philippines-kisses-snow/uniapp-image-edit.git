/**
 * 绘制箭头
 * @param {Object} ctx 画板
 * @param {Object} fromX 开始X坐标
 * @param {Object} fromY 开始y坐标
 * @param {Object} toX 结束X坐标
 * @param {Object} toY 结束y坐标
 * @param {Object} theta 三角斜边一直线夹角
 * @param {Object} headlen 三角斜边长度
 * @param {Object} width 箭头线宽度
 * @param {Object} color 箭头颜色
 */
export function drawArrow(ctx, fromX, fromY, toX, toY, theta, headlen, width, color, save = false) {
	theta = typeof(theta) != 'undefined' ? theta : 30;
	headlen = typeof(theta) != 'undefined' ? headlen : 10;
	width = typeof(width) != 'undefined' ? width : 1;
	color = typeof(color) != 'color' ? color : '#000';
	// 计算各角度和对应的P2,P3坐标
	var angle = Math.atan2(fromY - toY, fromX - toX) * 180 / Math.PI,
	angle1 = (angle + theta) * Math.PI / 180,
	angle2 = (angle - theta) * Math.PI / 180,
	topX = headlen * Math.cos(angle1),
	topY = headlen * Math.sin(angle1),
	botX = headlen * Math.cos(angle2),
	botY = headlen * Math.sin(angle2);
	ctx.save();
	ctx.beginPath();
	var arrowX = fromX - topX,
	arrowY = fromY - topY;
	ctx.moveTo(arrowX, arrowY);
	ctx.moveTo(fromX, fromY);
	ctx.lineTo(toX, toY);
	arrowX = toX + topX;
	arrowY = toY + topY;
	ctx.moveTo(arrowX, arrowY);
	ctx.lineTo(toX, toY);
	arrowX = toX + botX;
	arrowY = toY + botY;
	ctx.lineTo(arrowX, arrowY);
	ctx.strokeStyle = color;
	ctx.lineWidth = width;
	ctx.stroke();
	ctx.restore();
	if(save) {
		ctx.draw(true)
	} else {
		ctx.draw()
	}
}

/**
 * 计算缩放比
 * @param {Object} imgW 图片宽
 * @param {Object} imgH 图片高
 * @param {Object} conW 容器宽
 * @param {Object} conH 容器高
 * @returns {number} 缩放比
 */
export function getImgScale(imgW, imgH, conW, conH) {
	const IMG_RATIO = imgW / imgH
	const CON_RATIO = conW / conH
	let scale = 1
	if(IMG_RATIO >= CON_RATIO) {
		scale = conW / imgW
	} else if(IMG_RATIO < CON_RATIO) {
		scale = conH / imgH
	}
	return scale
}

/**
 * 绘制文本
 * @param {Object} _this 组件实例
 * @param {String} canvasID canvasID
 * @param {String} text 文本字符串
 * @param {Object} fontH 单行字体高度
 * @param {Object} maxW 最大文本域宽
 * @param {Object} startX 开始x坐标
 * @param {Object} startY 开始y坐标
 */
export function drawText(_this, canvasID, textArea, fontSize, fontH, maxW, startX, startY) {
	const ctx = uni.createCanvasContext(canvasID, _this)
	const text = textArea.text
	ctx.setFontSize(fontSize)
	ctx.fillStyle = textArea.color
	let rowW = 0
	let rowTexts = []
	let startIndex = 0
	for(let i = 0; i < text.length; i++) {
		rowW += ctx.measureText(text[i]).width
		if(rowW > maxW) {
			rowTexts.push(text.substr(startIndex, i))
			startIndex = i
			rowW = 0
		}
	}
	rowTexts.push(text.substr(startIndex))
	
	rowTexts.forEach((str, index) => {
		const y = index === 0 ? startY : index*fontH + startY
		ctx.fillText(str, startX, y);
		ctx.draw(true)
	})
}

export class TextArea {
	/**
	 * 位置信息
	 */
	location = {
		top: 0,
		left: 0,
		right: 0,
		bottom: 0
	}
	/**
	 * 文本内容
	 */
	text = ''
	/**
	 * 字体颜色
	 */
	color = ''
	
	/**
	 * @param {Number} areaW 文本域初始宽
	 * @param {Number} areaH 文本域初始高
	 * @param {Number} _W 容器宽
	 * @param {Number} _H 容器高
	 */
	constructor(areaW = 0, areaH = 0, _W = 0, _H = 0) {
		if(areaW && areaH && _W && _H) {
			this.location.left = (_W / 2) - (areaW / 2)
			this.location.right = (_W / 2) - (areaW / 2)
			this.location.top = (_H / 2) - (areaH / 2)
			this.location.bottom = (_H / 2) - (areaH / 2)
		}
	}
	
	/**
	 * @param {Object} loca 位置 ：{ top?: number; left?: number; right?: number; bottom?: number }
	 */
	setLocation(loca) {
		this.location = { ...this.location, ...loca }
	}
}


export const TOOL_TYPE = {
	/**
	 * 剪切
	 */
	CROP: 0,
	/**
	 * 文本
	 */
	TEXT: 1,
	/**
	 * 线条
	 */
	LINE: 2,
	/**
	 * 旋转
	 */
	ROTATE: 3,
	/**
	 * 几何
	 */
	DIAGRAM: 4
}

/**
 * 剪切蒙版
 */
export const CROP_TYPE = {
	/**
	 * 底部
	 */
	BOTTOM: 0,
	/**
	 * 左下
	 */
	BOTTOM_LEFT: 1,
	/**
	 * 右下
	 */
	BOTTOM_RIGHT: 2,
	/**
	 * 顶部
	 */
	TOP: 3,
	/**
	 * 左上
	 */
	TOP_LEFT: 4,
	/**
	 * 右上
	 */
	TOP_RIGHT: 5,
	/**
	 * 左侧
	 */
	LEFT: 6,
	/**
	 * 右侧
	 */
	RIGHT: 7
}

/**
 * 预置颜色
 */
export const COLOR = [
	'#FA3534',
	'#F9AE3D',
	'#000',
	'#2979FF'
]

/**
 * 预置字体大小
 */
export const LINE_SIZE = [4, 10, 16, 22]

/**
 * 预置图形
 */
export const DIAGRAM_TYPE = {
	/**
	 * 箭头
	 */
	ARROW: 0,
	/**
	 * 矩形
	 */
	RECTANGLE: 1,
	/**
	 * 圆形
	 */
	CIRCLE: 2,
	/**
	 * 椭圆
	 */
	OVAL: 3
}